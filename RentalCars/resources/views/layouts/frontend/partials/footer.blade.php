<div class="b-features">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-3 col-xs-6 col-xs-offset-6">
				<ul class="b-features__items">
					<li class="wow zoomInUp" data-wow-delay="0.3s"
						data-wow-offset="100">Low Prices, No Haggling</li>
					<li class="wow zoomInUp" data-wow-delay="0.3s"
						data-wow-offset="100">Largest Car Dealership</li>
					<li class="wow zoomInUp" data-wow-delay="0.3s"
						data-wow-offset="100">Multipoint Safety Check</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="b-info">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-xs-6">
				<aside class="b-info__aside wow zoomInLeft" data-wow-delay="0.3s">
					<article class="b-info__aside-article">
						<h3>OPENING HOURS</h3>
						<div class="b-info__aside-article-item">
							<h6>Sales Department</h6>
							<p>
								Mon-Sat : 8:00am - 5:00pm<br /> Sunday is closed
							</p>
						</div>
						<div class="b-info__aside-article-item">
							<h6>Service Department</h6>
							<p>
								Mon-Sat : 8:00am - 5:00pm<br /> Sunday is closed
							</p>
						</div>
					</article>
					<article class="b-info__aside-article">
						<h3>About us</h3>
						<p>Vestibulum varius od lio eget conseq uat blandit, lorem auglue
							comm lodo nisl non ultricies lectus nibh mas lsa Duis scelerisque
							aliquet. Ante donec libero pede porttitor dacu msan esct
							venenatis quis.</p>
					</article>
					<a href="about.html" class="btn m-btn">Read More<span
						class="fa fa-angle-right"></span></a>
				</aside>
			</div>
			<div class="col-md-4 col-xs-6">
				<div class="b-info__latest">
					<h3>LATEST AUTOS</h3>
					<div class="b-info__latest-article wow zoomInUp"
						data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-audi"></div>
						<div class="b-info__latest-article-info">
							<h6>
								<a href="detail.html">MERCEDES-AMG GT S</a>
							</h6>
							<p>
								<span class="fa fa-tachometer"></span> 35,000 KM
							</p>
						</div>
					</div>
					<div class="b-info__latest-article wow zoomInUp"
						data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-audiSpyder"></div>
						<div class="b-info__latest-article-info">
							<h6>
								<a href="#">AUDI R8 SPYDER V-8</a>
							</h6>
							<p>
								<span class="fa fa-tachometer"></span> 35,000 KM
							</p>
						</div>
					</div>
					<div class="b-info__latest-article wow zoomInUp"
						data-wow-delay="0.3s">
						<div class="b-info__latest-article-photo m-aston"></div>
						<div class="b-info__latest-article-info">
							<h6>
								<a href="#">ASTON MARTIN VANTAGE</a>
							</h6>
							<p>
								<span class="fa fa-tachometer"></span> 35,000 KM
							</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-5 col-xs-6">
				<address class="b-info__contacts wow zoomInUp" data-wow-delay="0.3s">
					<p>contact us</p>
					<div class="b-info__contacts-item">
						<span class="fa fa-map-marker"></span> <em>202 W 7th St, Suite 233
							Los Angeles, California 90014 USA</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-phone"></span> <em>Phone: 1-800- 624-5462</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-fax"></span> <em>FAX: 1-800- 624-5462</em>
					</div>
					<div class="b-info__contacts-item">
						<span class="fa fa-envelope"></span> <em>Email: info@domain.com</em>
					</div>
				</address>
				<address class="b-info__map">
					<a href="contacts.html">Open Location Map</a>
				</address>
			</div>
		</div>
	</div>
</div>
<!--b-info-->

<footer class="b-footer">
	<a id="to-top" href="#this-is-top"><i class="fa fa-chevron-up"></i></a>
	<div class="container">
		<div class="row">
			<div class="col-xs-5">
				<div class="b-footer__company wow fadeInLeft" data-wow-delay="0.3s">
					<a href="{{route('home')}}">
						<img src="{{asset('assets/frontend/images/logo/logo.png')}}" alt="logo" />
					</a>
					<p>Rental Cars &copy; 2018 Designed by Lương Quốc Đại &amp; 
					Đặng Đức Luân.</p>
				</div>
			</div>
			<div class="col-xs-7">
				<div class="b-footer__content wow fadeInRight" data-wow-delay="0.3s">
					<div class="b-footer__content-social">
						<a href="#"><span class="fa fa-facebook-square"></span></a> <a
							href="#"><span class="fa fa-google-plus-square"></span></a> <a
							href="#"><span class="fa fa-instagram"></span></a> <a href="#"><span
							class="fa fa-youtube-square"></span></a> <a href="#"><span
							class="fa fa-skype"></span></a>
					</div>
					<nav class="b-footer__content-nav">
						<ul>
							<li><a href="home.html">Home</a></li>
							<li><a href="404.html">Pages</a></li>
							<li><a href="listings.html">Inventory</a></li>
							<li><a href="about.html">About</a></li>
							<li><a href="404.html">Services</a></li>
							<li><a href="blog.html">Blog</a></li>
							<li><a href="listTable.html">Shop</a></li>
							<li><a href="contacts.html">Contact</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer>